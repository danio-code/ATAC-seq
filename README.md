# DANIO-CODE ATAC-seq pipeline v1.0

Assay for transposase-accessible chromatin using sequencing (ATAC-seq) uses
hypersensitive Tn5 transposase loaded in vitro with sequencing adapters for 
simultaneous fragmentation and tagging of the genome with sequencing adaptors. 
It is a widely used method for epigenetic profiling, including identification 
of open-chromatin regions, as well as nucleosome-bound and nucleosome-free 
positions (Buenrostro et al., 2013).

## Description of the pipeline

The ATAC-seq pipeline used in the analysis is an adapted 
ATAC-Seq / DNase-Seq Pipeline developed at Kundaje lab 
(https://github.com/kundajelab/atac_dnase_pipelines). The pipeline consists of 
the following steps:

* Quality filtering and trimming of raw reads (Trim-galore)
* Mapping reads to the reference genome (Bowtie2)
* Filtering of reads in the output bam file (samtools and sambamba)
* Marking optical duplicates (Picard tools)
* Deduplication (samtools and sambamba)
* Format conversion to a more manageable tagAlign format (https://genome.ucsc.edu/FAQ/FAQformat.html#format15)
* Tn5 shifting of aligned reads
* Peak calling (Macs2)

The analysis also include fragment length distribution of aligned read pairs,
which allows separation of reads into nucleosome-free region spanning, 
mononucleosome spanning, and di and more nucleosome spanning.
Such distribution of reads follows the formula:
<formula in LaTex>

The individual components of the function, as well as the fitted coefficients
are included as quality metrics in the results. All the analysis involving 
fragment length distribution are available as a R script in this
repository.

The script is called with one argument, a bam file. It outputs the pdf with the 
fragment length size distribution plot as wel as the fit curves and the fitted parameters.
It also outputs the mapped reads in bigWig format separated by insert size
(40 - 120 bp, 165 - 250 bp and 300 - 800 bp respectively). 

## Outputs
### Alignment files:

* Mapped reads
  * Format: bam    
  * File: *.bam
* Filtered and deduplicated reads 
  * Format: bam
  * File: *.nodup.bam
* Filtered and deduplicated reads
  * Format: tagAlign
  * File: *.tagAlign.gz
* Tn5 shifted tagAlign reads
  * Format: tagAlign
  * File: *.tn5.tagAlign.gz
* tagAlign reads subsampled to 25M
  * Format: tagAlign
  * File: *.25M.tagAlign.gz

### Signal files:

* All signal files are in bigWig format (extension .bw or .bigWig)
* Read pairs in the nucleosome-free region 
  * File: *.nsomeFree.bw 
* Read pairs spanning one nucleosome
  * File: *.monoNsome.bw
* Read pairs spanning more than one nucleosome
  * File: *.polyNsome.bw
* Fold change signal tracks
  * File: *.fc.signal.bigwig 
* P-val signal tracks
  * File: *.pval.signal.bigwig 

### Peak files:

* Narrow peaks (p-val threshold = 0.1)
  * File: *.pval0.1.narrowPeak.gz
* Gapped peaks (p-val threshold = 0.1)
  * File: *.pval0.1.gappedPeak.gz
* Broad peaks (p-val threshold = 0.1)
  * File: *.pval0.1.broadPeak.gz
* Narrow peaks (p-val threshold = 0.01)
  * File: *.narrowPeak.gz
* Gapped peaks (p-val threshold = 0.01)
  * File: *.gappedPeak.gz
* Broad peaks (p-val threshold = 0.01)
  * File: *.broadPeak.gz
* 500K strongest narrow peaks (p-val threshold = 0.1)
  * File: *.pval0.1.500K.narrowPeak.gz
* 500K strongest gapped peaks (p-val threshold = 0.1)
  * File: *.pval0.1.500K.gappedPeak.gz

### QC and log files:
* All qc and log files, including fragment length distribution plot with fitted curve and calculated parameters in a pdf file  
   * Qc files include:
     * samtools flagstat statistics before and after deduplication (*.flagstat.qc)
     * deduplication stats (*.dup.qc)
     * PCR Bottleneck Coefficient (*.pbc.qc)
       * The *.pbc.qc file is a tab delimited text described in the table below
   * Log files include:
     * adapter sequences (*.adapter.txt)
     * read length (*.read_length.txt)
     * log file from the aligner (*.align.log)

| Total read pairs | Distinct read pairs | One read pair | Two read pairs | NRF                     | PBC1                         | PBC2                         |
|:----------------:|:-------------------:|:-------------:|:--------------:|:-----------------------:|:----------------------------:|:----------------------------:|
|                  |                     |               |                | Non Redundant Fraction  | PCR Bottleneck Coefficient 1 | PCR Bottleneck Coefficient 2 |

NRF = Distinct read pairs / Total read pairs  
PBC1 = One read pair / Distinct read pairs  
PBC2 = One read pair / Two read pairs

PBC1 is the primary measure. Provisionally:
   * 0 - 0.5   --> severe bottlenecking
   * 0.5 - 0.8 --> moderate bottlenecking
   * 0.8 - 0.9 --> mild bottlenecking
   * 0.9 - 1.0 --> no bottlenecking
   

